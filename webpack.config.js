const webpack = require('webpack');
const path = require('path');

const BrowserSyncPlugin = require('browser-sync-webpack-plugin');

const PARENT_PATH = path.resolve(__dirname, 'src/client/');
const SOURCE_PATH = path.resolve(__dirname, 'src/client/app/');

module.exports = {
    entry: SOURCE_PATH + '/index.jsx',
    output: {
        path: PARENT_PATH,
        filename: 'public/bundle.js',
        hotUpdateChunkFilename: 'public/[id].[hash].hot-update.js',
        hotUpdateMainFilename: 'public/[hash].hot-update.json'
    },
    module: {
        rules:
        [
            {
                test: /\.jsx?/,
                include: SOURCE_PATH,
                use: "babel-loader"
            },
            {
                test: /\.scss$/,
                use:
                [
                    {
                        loader: "style-loader"
                    },
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            localIdentName: "[name]__[local]___[hash:base64:5]"
                        }
                    },
                    {
                        loader: "sass-loader"
                    }
                ]
            },
            {
                test: /\.(png|jp(e*)g|svg)$/,
                use: [{
                    loader: 'url-loader'
                }]
            }
        ]
    },
    resolve: {
        modules: [
            path.resolve(__dirname, 'node_modules'),
            '/usr/share/javascript',
            '/usr/lib/nodejs',
        ],
    },
    devServer: {
      proxy: {
        '/api': {
          target: 'http://localhost:8080',
          secure: false,
          changeOrigin: true
        }
      },
      port: 8081,
      host: '0.0.0.0',
      publicPath: 'http://localhost:8081/',
      contentBase: PARENT_PATH
    }
};
