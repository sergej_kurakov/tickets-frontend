FROM node:8 as build-stage

WORKDIR /tmp
COPY package.json /tmp/
RUN npm install

WORKDIR /usr/src/app
COPY . /usr/src/app/
RUN cp -a /tmp/node_modules /usr/src/app/

RUN npm run build

ENV NODE_ENV=production
ENV PORT=4000

EXPOSE 4000
CMD [ "npm", "run", "start" ]