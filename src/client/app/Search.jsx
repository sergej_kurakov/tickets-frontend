import React from 'react';
import styles from './Search.scss';

class Search extends React.Component {
	
	constructor(props) {
		super(props);

		this.handleSearch = this.handleSearch.bind(this);
	}

	handleSearch(event) {
		this.props.handleSearch(event.target.value);
	}

	componentDidMount() {
		this.props.handleSearch('');
	}

	render () {
		return (
			<div className={styles.search}>
				<input type="text" onChange={this.handleSearch}/>
			</div>
		);
	}
}

export default Search;