import React from 'react';
import { hot } from 'react-hot-loader/root'
import Search from './Search.jsx';
import List from './List.jsx';
import styles from './style.scss';
import { Redirect } from 'react-router-dom';
import { BrowserRouter } from 'react-router-dom';

class App extends React.Component {

	constructor(props) {
		super(props);
		this.state = { list: [] };

		this.search = this.search.bind(this);
		this.purchase = this.purchase.bind(this);
	}

	search(text) {
		fetch('/api/events?text=' + text)
			.then(data => data.json())
			.then(data => this.setState({ list: data }));
    }

    purchase(identifier) {
    	fetch('/api/purchase/' + identifier)
    		.then(data => data.json())
    		.then(data => {
    			console.log(data);
    		    this.setState({ redirectUrl: data.redirect_url });
		    })
    }

    render () {
    	if (this.state.redirectUrl) {
    	    window.location = this.state.redirectUrl;
    	}
    	return (
        	<div>
        		<nav>
	        		<div className={styles.search}>
	        			<Search handleSearch={this.search} />
	        		</div>
	        	</nav>
	        	<div className={styles.app}>
	        		<List list={this.state.list} purchase={this.purchase} />
	            </div>
            </div>
        );
    }
}

export default hot(App);