import React from 'react';
import styles from './List.scss';
import PaypalButton from './PaypalButton.jsx'

class List extends React.Component {

	constructor(props) {
		super(props);

		this.purchase = this.purchase.bind(this);
	}

	purchase(identifier) {
		this.props.purchase(identifier);
	}

	render () {
		return (
			<ul className={styles.list}>
				{this.props.list.map((item, index) => 
                	<li key={index}>
                		<div className={styles.item}>
						  <img src={item.image} alt="Card image cap"/>
						  <div style={{ padding: '20px 12px 20px 12px' }}>
						    <h5>{item.title}</h5>
						    <p>{item.description}</p>
						  	<PaypalButton id={item.id}/>
						  </div>
						</div>
                	</li>
				)}
			</ul>
		);
	}
}

export default List;