import React from 'react';
import ReactDOM from 'react-dom';
import scriptLoader from 'react-async-script-loader';

class PaypalButton extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showButton: false,
    };

    window.React = React;
    window.ReactDOM = ReactDOM;
  }

  componentDidMount() {
    const {
      isScriptLoaded,
      isScriptLoadSucceed
    } = this.props;

    if (isScriptLoaded && isScriptLoadSucceed) {
      this.setState({ showButton: true });
    }
  }

  componentWillReceiveProps(nextProps) {
    const {
      isScriptLoaded,
      isScriptLoadSucceed,
    } = nextProps;

    const isLoadedButWasntLoadedBefore =
      !this.state.showButton &&
      !this.props.isScriptLoaded &&
      isScriptLoaded;

    if (isLoadedButWasntLoadedBefore) {
      if (isScriptLoadSucceed) {
        this.setState({ showButton: true });
      }
    }
  }

  render() {
    const {
      showButton,
    } = this.state;

    const payment = () => {
      var CREATE_URL = '/api/pay?id=' + this.props.id;

      return paypal.request.post(CREATE_URL)
        .then(function(res) {
            return res.id;
        });
    }

    const onAuthorize = (data) => {
      console.log('onAuthorize');

      var EXECUTE_URL = '/api/pay/success';

      var data = {
          paymentID: data.paymentID,
          payerID: data.payerID
      };

      return paypal.request.post(EXECUTE_URL, data)
        .then(function (res) {
            console.log('Payment complete')
        });
    }

    return (
      <div>
        {showButton && <paypal.Button.react
          env={'sandbox'}
          commit={true}
          payment={payment}
          onAuthorize={onAuthorize}
          style={{
            size: 'responsive',
            shape: 'rect',
            color: 'blue'
          }}
        />}
      </div>
    );
  }
}

export default scriptLoader('https://www.paypalobjects.com/api/checkout.js')(PaypalButton);